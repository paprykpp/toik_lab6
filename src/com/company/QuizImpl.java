package com.company;

public class QuizImpl implements Quiz{
    private int digit;

    public QuizImpl() {
        this.digit = 254;
    }

    @Override
    public void isCorrectValue(int value) throws ParamTooLarge, ParamTooSmall {
        if(digit > value){
            throw new ParamTooSmall();
        }else if(digit < value){
            throw new ParamTooLarge();
        }
    }
}
