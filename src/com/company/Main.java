package com.company;

public class Main {

    public static void main(String[] args) {
        Quiz quiz = new QuizImpl();
        int minVal = Quiz.MIN_VALUE;
        int maxVal = Quiz.MAX_VALUE;
        int digit = (minVal+maxVal)/2;

        for(int counter = 1; ;counter++) {
            try {
                quiz.isCorrectValue(digit);
                System.out.println("Trafiona proba!!! Szukana liczba to: "
                        + digit + " Ilosc prob: " + counter);
                break;
            } catch (Quiz.ParamTooLarge paramTooLarge) {
                System.out.println("Argument za duzy!!!");
                maxVal = digit;
                digit = (minVal + maxVal)/2;
            } catch (Quiz.ParamTooSmall paramTooSmall) {
                System.out.println("Argument za maly!!!");
                minVal = digit;
                digit = (minVal + maxVal)/2;
            }
        }
    }
}
